import React, { useState, useContext, useRef, useEffect } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import Search from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import Typography from '@material-ui/core/Typography';
import { Button, IconButton, Tooltip } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import { styles } from './styles_PDFViewer';

import { ContextMainEditor } from '../../../views/MainEditorView/ContextMainEditor';

pdfjs.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const useStyles = makeStyles(styles);

const PDFViewer = (props) => {
	const { positionCss } = props;
	const classes = useStyles();
	const { actulaDocument, actulaDocumentRegistro } = useContext(ContextMainEditor);
	const ref = useRef();

	const [numPages, setNumPages] = useState(null);
	const [pageNumber, setPageNumber] = useState(1);
	const [pages, setPages] = useState("");
	const [fileHeader, setFileHeader] = useState("NO SET");

	/* console.log(actulaDocument); */
	function onDocumentLoadSuccess({ numPages }) {
		setNumPages(numPages);
		const pSet = new Array(numPages).fill(1).map(
			(i, n) => (
				<Page
					key={`page_${n}`}
					/* width={(window.outerWidth) / 2} */
					className={classes.page}
					pageNumber={n + 1}
				/>
			)
		);
		setPages(pSet);
	}
	useEffect(() => {
		if (actulaDocumentRegistro !== undefined) {
			console.log(actulaDocumentRegistro);
			setFileHeader(actulaDocumentRegistro.tipo_documento)
		}
	}, [actulaDocumentRegistro])

	return (
		<div className={`${positionCss}`}>
			<Card className={classes.root}>
				<CardContent className={classes.container}>
		{/*<Typography variant="h5" component="h1">
						{fileHeader}
					</Typography>*/}
					<div ref={ref} className={classes.toolBar}>
		{/*<ToolBar></ToolBar>*/}
					</div>
					<Document

						file={actulaDocument}
						onLoadSuccess={onDocumentLoadSuccess}
						className={classes.content}
					>
						{pages}
					</Document>
		{/*<p>Page {pageNumber} of {numPages}</p>*/}
				</CardContent>
			</Card>
		</div>
	)
}

const ToolBar = ({ zoomOut, zoomIn }) => {
	const classes = useStyles();
	return (
		<>
			<Tooltip title="Procesar archivo con OCR">
				<Button className={classes.button} variant="contained" color="primary">
					OCR
            </Button>
			</Tooltip>
			<div className="">
				<Tooltip title="Buscar">
					<IconButton size="medium" >
						<Search />
					</IconButton>
				</Tooltip>
				<InputBase
					className={classes.input}
					placeholder="Buscar Texto"
					inputProps={{ 'aria-label': 'search google maps' }}
				/>

			</div>

			<Tooltip title="Anonimizar">
				<IconButton size="small">
					<VisibilityOffIcon />
				</IconButton>
			</Tooltip>

			<Tooltip title="Alejar">
				<IconButton size="small">
					<ZoomOutIcon />
				</IconButton>
			</Tooltip>
			<Tooltip title="Acercar">
				<IconButton size="small" >
					<ZoomInIcon />
				</IconButton>
			</Tooltip>
		</>
	)
}


export default PDFViewer;
