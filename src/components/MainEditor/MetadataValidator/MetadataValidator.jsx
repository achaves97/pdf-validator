import React, { useContext, useState, useEffect } from 'react';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import { styles, deepStyles } from './styles_MetadataValidator';
import { ContextMainEditor } from '../../../views/MainEditorView/ContextMainEditor';

const useStyles = makeStyles(styles);
const TextFieldDeepStyles = withStyles(deepStyles)(TextField);

const MetadataValidator = (props) => {
    const classes = useStyles();
    const { positionCss } = props;
    const { actulaDocumentMetadata } = useContext(ContextMainEditor);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (actulaDocumentMetadata) {
            setLoading(false)
        }
    }, [actulaDocumentMetadata])

    return (
        <div className={`${positionCss}`}>
            <Card className={classes.root}>
                <CardContent className={classes.content}>

                    <Typography variant="h5" component="h1">
                        {" Metadata Extraida"}
                    </Typography>

                    {
                        loading ?
                            <>loading</> :
                            <MetadataInputsSet fieldsSet={actulaDocumentMetadata} />
                    }
                </CardContent>
                <CardActions className={classes.submit}>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary">
                        APRUEBA
                    </Button>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="secondary">
                        RECHAZA
                    </Button>
                </CardActions>
            </Card>
        </div>
    )
}

const MetadataInputsSet = (props) => {
    const { fieldsSet } = props;
    const classes = useStyles();

    /* console.log(fieldsSet); */
    const inputs = Object.keys(fieldsSet).map((key, index) => {
        /*  console.log(key); */
        return (
            <form key={key} className={classes.form} noValidate autoComplete="off">
                <Typography className={classes.index} color="textPrimary" gutterBottom>
                    {index}
                </Typography>

                <TextFieldDeepStyles
                    className={classes.input}
                    id="outlined-basic"
                    label={key}
                    defaultValue={fieldsSet[key]}
                    variant="outlined"
                    fullWidth
                />
            </form>
        )
    })
    return (
        <div className={classes.inputContainer}>
            {inputs}
        </div>
    )
}
MetadataInputsSet.defaultProps = {
    fieldsSet: { name: "EMPTY OBJECT" }
}


export default MetadataValidator;