

export const styles = {
    root: {
      height: "calc(100% - 24px)",
      margin: "10px",
      padding: "0 0 5% 0"
    },
    content:{
        height:"90%",
        display: "grid",
    },
    form: {
        width: "100%",
        margin: "10px 0",
        display: "grid",
        gridTemplateColumns: "max-content 1fr",
        placeItems: "center",    
       
        padding: "7px",
        borderRadius: "5px",
        '&:nth-child(2n)': {
            background: "#ddd",  
        }
    },
    inputContainer:{
       overflow: "auto" ,
    },
    index:{
        display: "grid",
        placeItems: "center",
        color: "white",
        margin: 0,
        width: "30px",
        height: "30px",
        fontSize: ".8em",
        background: "var(--my-Color)",
        marginRight: "1em",
        borderRadius: "50%"
    },
    input: {
        
    }
  };
  
  export const deepStyles = {
    root: {
        '& .MuiOutlinedInput-input': {
            padding: "16px 12px 12px 12px",
            
        }
    }
  }