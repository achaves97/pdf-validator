import React, { useState, useEffect, useContext } from 'react';

import { ContextAppSession } from '../../ContextAppSession';
import { ContextMainEditorProvider, ContextMainEditor } from './ContextMainEditor';

import PDFViewer from '../../components/MainEditor/PDFViewer/PDFViewer';
import DocumentRegister from '../../components/MainEditor/DocumentRegister/DocumentRegister';
import MetadataValidator from '../../components/MainEditor/MetadataValidator/MetadataValidator';

import { requestDocumentAndDocumentMetadata } from '../../services/MainEditorServices';

import './styles_MainEditorView.css';

const MainEditorViewWhitContext = () => {
  return (
    <ContextMainEditorProvider>
      <MainEditorView></MainEditorView>
    </ContextMainEditorProvider>
  )
}


function MainEditorView() {
  const { setActulaDocument,
    setActulaDocumentRegistro,
    setActulaDocumentMetadata, } = useContext(ContextMainEditor);

  const { assignedDocuments, token } = useContext(ContextAppSession);

  useEffect(() => {
    /*  console.log(contextMainEditor); */
    console.log(assignedDocuments);
    if (assignedDocuments) {
      const config = {
        asignado: 44112233,
        token
      }
      requestDocumentAndDocumentMetadata(config).then(result => {
        const { pdf_procesado, registro, metadata } = result;
        console.log(result);
        if (Object.keys(result).length > 0) {
          const PDF = `data:application/pdf;base64,${pdf_procesado}`
          /*  console.log(PDF); */
          setActulaDocument(PDF);
          setActulaDocumentRegistro(registro);
          setActulaDocumentMetadata(metadata);
        }
      })
    }
  }, [assignedDocuments]);

  return (
    <div className="main-editor">
      <PDFViewer positionCss={"pdf-viewer"} />
      <DocumentRegister positionCss={"document-register"} />
      <MetadataValidator positionCss={"metadata-validator"} />
    </div>
  );
}

export default MainEditorViewWhitContext;
