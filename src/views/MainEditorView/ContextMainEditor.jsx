import React, { useState, createContext, useEffect } from "react";

const ContextMainEditor = createContext(); // el q se exporta 


const ContextMainEditorProvider = ({ children }) => {
    const [actulaDocument, setActulaDocument] = useState("");
    const [actulaDocumentRegistro, setActulaDocumentRegistro] = useState();
    const [actulaDocumentMetadata, setActulaDocumentMetadata] = useState();

    useEffect(() => {

    }, [])

    return (
        <ContextMainEditor.Provider
            value={
                {
                    //Variables
                    actulaDocument,
                    actulaDocumentRegistro,
                    actulaDocumentMetadata,

                    //Mutators
                    setActulaDocument,
                    setActulaDocumentRegistro,
                    setActulaDocumentMetadata,
                }
            }
        >
            {children}
        </ContextMainEditor.Provider>
    )
}

export { ContextMainEditorProvider, ContextMainEditor }; 